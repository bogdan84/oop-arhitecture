<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 9:48 PM
 */
namespace MyEAccount\Containers;

class Helpers
{
    public $date;

    public function __construct()
    {
        $this->date = new \DateInterval("P2Y4DT6H8M");
    }
}
