<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 11:10 PM
 */

namespace MyEAccount\DataSource;

use MyEAccount\DataSource\Cache\Cache;
use MyEAccount\DataSource\CollectorLogic\Collector;
use MyEAccount\DataSource\Entities\GetMenu;
use MyEAccount\DataSource\File\File;

require_once "File/File.php";
require_once "Cache/Cache.php";
require_once "CollectorLogic/Collector.php";

class DataSource
{
    public function getMenu(): GetMenu
    {
        return (new Collector(new Cache(new File())))->getMenu();
    }
}