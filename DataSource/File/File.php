<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 9:48 PM
 */
namespace MyEAccount\DataSource\File;

use MyEAccount\DataSource\DataSourceInterface;

require_once __DIR__ . "\..\DataSourceInterface.php";

class File
    implements DataSourceInterface
{

     public function getMenu() :array
     {
         return [
             'www' => [
                 [
                     'name' => 'Date personale',
                     'url' => '/user/account',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Setări siguranță',
                     'url' => '/user/security',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'My Orders',
                     'url' => '/history/shopping',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'eMAG Card',
                     'url' => '/user/card-emag',
                     'flag' => 'new',
                     'site_module' => false,
                     'country' => ['ro',],
                 ],
                 [
                     'name' => 'My cards',
                     'url' => '/user/cards',
                     'flag' => false,
                     'site_module' => 'card_token_on_site_enabled',
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'My subscriptions',
                     'url' => '/user/subscriptions',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Garanțiile mele',
                     'url' => '/user/warranties',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu',],
                 ],
                 [
                     'name' => 'Return',
                     'url' => '/user/return',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Service',
                     'url' => '/user/service',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Vouchers & gift cards',
                     'url' => '/user/vouchers',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Review-urile mele',
                     'url' => '/user/reviews',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Favorite',
                     'url' => '/favorites',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Shopping list',
                     'url' => '/supermarket/favorites',
                     'flag' => false,
                     'site_module' => 'supermarket_enabled',
                     'country' => ['ro',],
                 ],
             ],
             'm' => [
                 [
                     'name' => 'Date personale',
                     'url' => '/user/account',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Setări siguranță',
                     'url' => '/user/security',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'My Orders',
                     'url' => '/history/shopping',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'eMAG Card',
                     'url' => '/user/card-emag',
                     'flag' => 'new',
                     'site_module' => false,
                     'country' => ['ro',],
                 ],
                 [
                     'name' => 'My cards',
                     'url' => '/user/cards',
                     'flag' => false,
                     'site_module' => 'card_token_on_site_enabled',
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'My subscriptions',
                     'url' => '/user/subscriptions',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Adaugă/Editează adrese',
                     'url' => '/user/listaddresses',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Adaugă/Editează firme',
                     'url' => '/user/listcompanies',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Garanțiile mele',
                     'url' => '/user/warranties',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu',],
                 ],
                 [
                     'name' => 'Return',
                     'url' => '/user/return',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Service',
                     'url' => '/user/service',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Vouchers & gift cards',
                     'url' => '/user/vouchers',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Review-urile mele',
                     'url' => '/user/reviews',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Favorites',
                     'url' => '/favorites',
                     'flag' => false,
                     'site_module' => false,
                     'country' => ['ro', 'bg', 'hu', 'pl',],
                 ],
                 [
                     'name' => 'Shopping list',
                     'url' => '/supermarket/favorites',
                     'flag' => false,
                     'site_module' => 'supermarket_enabled',
                     'country' => ['ro',],
                 ],
             ],
         ];
     }
}
