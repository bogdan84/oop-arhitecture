<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/26/2019
 * Time: 7:51 PM
 */
namespace MyEAccount\DataSource\Entities;
use MyEAccount\DataSource\Entities\GetMenuType;

class GetMenu
{
    /**
     * @var GetMenuType[]
     */
    public $www=[];

    /**
     * @var GetMenuType[]
     */
    public $m=[];
}