<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/26/2019
 * Time: 7:52 PM
 */
namespace MyEAccount\DataSource\Entities;

class GetMenuType
{
    public $name;
    public $url;
    public $flag;
    public $site_module;
    public $country;
}