<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/26/2019
 * Time: 6:37 PM
 */

namespace MyEAccount\DataSource;

interface DataSourceInterface
{
    public function getMenu() :array;
}
