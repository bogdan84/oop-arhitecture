<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/26/2019
 * Time: 1:55 AM
 */
namespace MyEAccount\DataSource\Cache;

use MyEAccount\DataSource\DataSourceInterface;

class Cache
    implements DataSourceInterface
{
    private $dataSource;

    private $tempData = [];

    private $resetCache;

    public function __construct(DataSourceInterface $dataSource, bool $resetCache = true)
    {
        $this->dataSource = $dataSource;
        $this->resetCache = $resetCache;
    }

    public function getMenu() :array
    {
        if($this->resetCache){
            $this->tempData = $this->dataSource->getMenu();
        }

        return $this->tempData;
    }

}