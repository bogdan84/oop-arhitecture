<?php
namespace MyEAccount\DataSource\CollectorLogic;

use MyEAccount\DataSource\DataSourceInterface;
use MyEAccount\DataSource\Entities\GetMenu;
use MyEAccount\DataSource\Entities\GetMenuType;

require_once __DIR__ . "\..\Entities\GetMenu.php";
require_once __DIR__ . "\..\Entities\GetMenuType.php";


class Collector
{
    private $dataSource;

    public function __construct(DataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function getMenu()
    {
        $getEntity = new GetMenu();
        foreach ($this->dataSource->getMenu() as $key => $val) {
            foreach ($val as  $itemType) {
                $getTypeEntity = new GetMenuType();
                foreach ($itemType as $keyType => $item) {
                    if (property_exists($getTypeEntity, $keyType)) {
                        $getTypeEntity->$keyType = $item;
                    }
                }
                if (property_exists($getEntity, $key)) {
                    $getEntity->$key[] = $getTypeEntity;
                }
            }
        }

        return $getEntity;
    }
}