<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 9:56 PM
 */
namespace MyEAccount\BusinessLogic;

use MyEAccount\Containers\Helpers;
use MyEAccount\DataSource\Entities\GetMenu;
use MyEAccount\Response\GetMenuEntity;
use MyEAccount\Request\GetMenuEntity as GetMenuEntityRequest;
use MyEAccount\Response\GetMenuTypeEntity;

require_once __DIR__ . "/../Response/GetMenuEntity.php";
require_once __DIR__ . "/../Response/GetMenuTypeEntity.php";

class BusinessLogic
{
    private $helpers;

    public function __construct(Helpers $helpers)
    {
        $this->helpers = $helpers;
    }

    public function getMenu(GetMenuEntityRequest $request, GetMenu $dataSourceEntity)
    {
        $response = new GetMenuEntity();

        $pathInfo =  parse_url($request->currentURL,PHP_URL_PATH);
        $host =  explode('.', parse_url($request->currentURL,PHP_URL_HOST));
        $platform = $host[0];
        $wasSelected = false;

        foreach ($dataSourceEntity->$platform as $valueDataSource)
        {
            if(in_array($host[2], $valueDataSource->country)) {
                $selected = (rtrim($pathInfo, '/') == $valueDataSource->url) ? true : false;
                $wasSelected = $selected ? true : $wasSelected;

                $responseItem = new GetMenuTypeEntity();
                $responseItem->name = $valueDataSource->name;
                $responseItem->url = $valueDataSource->url;
                $responseItem->flag = $valueDataSource->flag;
                $responseItem->selected = $selected;

                $response->menu[] = $responseItem;
            }
        }

        if(!$wasSelected){
            $response->menu[0]->selected = true;
        }

        return $response;

    }

}