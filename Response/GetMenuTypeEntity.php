<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/27/2019
 * Time: 3:06 PM
 */

namespace MyEAccount\Response;


class GetMenuTypeEntity
{
    public $name;

    public $url;

    public $selected = false;

    public $flag = false;
}
