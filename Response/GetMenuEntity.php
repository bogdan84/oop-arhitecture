<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 10:24 PM
 */
namespace MyEAccount\Response;

class GetMenuEntity
{
    /**
     * @var array
     */
    public $menu = [];
}
