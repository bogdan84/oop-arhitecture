<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 9:30 PM
 */

namespace MyEAccount;

use MyEAccount\BusinessLogic\BusinessLogic;
use MyEAccount\DataSource\DataSource;
use MyEAccount\Request\GetMenuEntity as GetMenuEntityRequest;

require_once "DataSource/DataSource.php";
require_once "BusinessLogic/BusinessLogic.php";
require_once "MyAccountAbstract.php";
require_once "Request/GetMenuEntity.php";

class MyAccount   extends MyAccountAbstract
{

    public function getMenu()
    {
        $request = new GetMenuEntityRequest();
        return (new BusinessLogic($this->helpers))
            ->getMenu(
                $request,
                (new DataSource())->getMenu()
            );
    }

}

$resp = (new MyAccount)->getMenu();
var_dump($resp);