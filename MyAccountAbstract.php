<?php
/**
 * Created by PhpStorm.
 * User: bogdan.stoica
 * Date: 1/25/2019
 * Time: 9:59 PM
 */
namespace MyEAccount;

use MyEAccount\Containers\Helpers;

require_once "Helpers/Helpers.php";

abstract class MyAccountAbstract
{
    public $helpers;

    public function __construct()
    {
        $this->helpers = new Helpers();
    }

    abstract public function getMenu();


}